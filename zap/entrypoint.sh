#!/usr/bin/env bash


sudo -u zap bash -c "/home/zap/ZAP/zap.sh \
  -daemon \
  -host 0.0.0.0 \
  -port 8086 \
  -config api.addrs.addr.name='.*' \
  -config api.addrs.addr.regex=true \
  -config api.disablekey=true \
  -certload /home/zap/ca.pem"

tail -f /dev/null

